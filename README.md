# README #

Stocktwits is a demo Android application which displays a Stocktwits feed in real time using the streamdata.io proxy.

### Getting started ###
To run the sample, you can clone this GitHub repository, and then open the project with Android Studio.

### Dependencies ###
* To display images from an url: [ION](https://github.com/koush/ion#get-ion) & [Picasso](http://square.github.io/picasso/)
* To translate HTML: [Jakarta Commons Lang](https://commons.apache.org/proper/commons-lang/download_lang.cgi)

Dependencies for using streamdata.io:

* JSON library: [Jackson](https://github.com/FasterXML/jackson-databind)
* JSON patch: [JSON Patch](https://github.com/fge/json-patch)
* SSE: [Eventsource-android](https://github.com/tylerjroach/eventsource-android)