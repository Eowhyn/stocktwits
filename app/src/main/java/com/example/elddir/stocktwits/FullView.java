package com.example.elddir.stocktwits;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;


public class FullView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_view);

        Intent i = getIntent();
        String[] mArray = i.getStringArrayExtra("strings");

        ((TextView) findViewById(R.id.text)).setText(mArray[0]);
        ((TextView) findViewById(R.id.date_time)).setText(mArray[1]);

        if (mArray[2].isEmpty()){
            // Here we avoid loading a default picture from network or disk memory
            // and leave the imageView empty
            ((ImageView) findViewById(R.id.pic)).setImageDrawable(null);
        } else {
            Picasso.with(FullView.this)
                    .load(mArray[2])
                    .into((ImageView) findViewById(R.id.pic));
        }

        ((TextView) findViewById(R.id.username)).setText(mArray[3]);
        ((TextView) findViewById(R.id.name)).setText(mArray[4]);
        if(!"null".equals(mArray[5])) {
            ((TextView) findViewById(R.id.bio)).setText(mArray[5]);
        }
        ((TextView) findViewById(R.id.joined)).setText("Joined on " + mArray[6]);
        // avatar
        Ion.with((ImageView) findViewById(R.id.avatar))
                .placeholder(R.drawable.defaulticon)
                .error(R.drawable.defaulticon)
                .load(mArray[7]);

        if(!"null".equals(mArray[8]) && !mArray[8].isEmpty()) {
            ((TextView) findViewById(R.id.location)).setText(mArray[8]);
        }
        ((TextView) findViewById(R.id.followers)).setText(mArray[9] + " followers");
        ((TextView) findViewById(R.id.following)).setText("Following " + mArray[10]);
        ((TextView) findViewById(R.id.identity)).setText(mArray[11]);
        if(!"null".equals(mArray[12])) {
            ((TextView) findViewById(R.id.experience)).setText(mArray[12]);
        }
        if(!"null".equals(mArray[13])) {
            ((TextView) findViewById(R.id.holding_period)).setText(mArray[13]);
        }
        if(!"null".equals(mArray[14])) {
            ((TextView) findViewById(R.id.approach)).setText(mArray[14] + " Approach");
        }
        if(!"null".equals(mArray[15])) {
            ((TextView) findViewById(R.id.website)).setText("Website: " + mArray[15]);
        }
        ((TextView) findViewById(R.id.ideas)).setText(mArray[16] + " ideas");

    }



    // Toolbar
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_full_view, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
