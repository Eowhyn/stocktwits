package com.example.elddir.stocktwits;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import tylerjroach.com.eventsource_android.EventSource;
import tylerjroach.com.eventsource_android.EventSourceHandler;
import tylerjroach.com.eventsource_android.MessageEvent;


public class MainActivity extends AppCompatActivity {
    private String TAG = "ACTIVITY";

    private String sdProxyPrefix = "https://streamdata.motwin.net/";
    private String sdToken = "Mjg4N2YwOWItMWQxNS00ZTM0LTkwMDQtODRiZDEwMGM1ZGU2";
    private String api ="https://api.stocktwits.com/api/2/streams/symbol/EURUSD.json";
    /* NOTE: this is an unauthenticated API call
     * 200 calls/hour are allowed */

    private ArrayList<Tweet> tweets;

    private final ObjectMapper mapper = new ObjectMapper();
    private JsonNode data;
    EventSource eventSource;

    ListView mlistview;
    MyListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mlistview = (ListView) findViewById(R.id.listView);
        adapter = new MyListAdapter(MainActivity.this, tweets);
        mlistview.setAdapter(adapter);

        tweets = new ArrayList<Tweet>();

        mlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tweet t= tweets.get(position);
                String[] mStrArr = {t.body, t.date_time, t.imgURL,
                                    t.user.username, t.user.name, t.user.bio,
                                    t.user.join, t.user.avatarURL, t.user.location,
                                    t.user.followers, t.user.following, t.user.off,
                                    t.user.xplvl, t.user.holding_p, t.user.approach,
                                    t.user.webURL, t.user.ideas};

                Intent i = new Intent(MainActivity.this, FullView.class);
                i.putExtra("strings", mStrArr);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Picasso.with(this).cancelTag(this);
    }


    // This should be done in a thread
    protected void connect() {
        // Detect network connection
        ConnectivityManager cm = (ConnectivityManager)getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected) {
            Map<String, String> header = new HashMap<String, String>();
            header.put("X-Sd-Token", sdToken);
            try {
                eventSource = new EventSource(new URI(sdProxyPrefix), new URI(api), new SSEHandler(), header);
                // Start receiving data
                eventSource.connect();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            "No network connection",
                            Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    protected void disconnect(){
        if(eventSource != null && eventSource.isConnected())
            eventSource.close();
    }




    /* ***************************** Class SSEHandler ***************************** */
    private class SSEHandler implements EventSourceHandler {

        public SSEHandler() {
        }

        @Override
        public void onConnect() {
            Log.v("SSE Connected", "True");
        }

        @Override
        public void onMessage(String event, MessageEvent message) {
            Log.v("SSE Message", event);
            if ("data".equals(event)) {
                try {
                    data = mapper.readTree(message.data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                updateList();
            } else if ("patch".equals(event)) {
                try {
                    JsonNode patchNode = mapper.readTree(message.data);
                    JsonPatch patch = JsonPatch.fromJson(patchNode);
                    data = patch.apply(data);
                    // Call GC to avoid unnecessary mem overload
                    //System.gc();
                    updateList();
                    runOnUiThread(new Runnable() {
                        //@Override
                        public void run() {
                            // Alert user of new activity
                            Toast toast = Toast.makeText(getApplicationContext(),
                                    "New recent activity",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.TOP, 0, 0);
                            toast.show();
                        }
                    });
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JsonPatchException e) {
                    e.printStackTrace();
                }

            } else {
                // If error is due to too many calls to API make a toast instead of having app crash
                //Log.i(TAG, "message: " + message.toString());
                if(message.toString().contains("429 Unknown Error")) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Too many API calls. Please close app and try again later.",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                    disconnect();
                } else {
                    throw new RuntimeException("Unexpected SSE message: " + event);
                }
            }
        }

        @Override
        public void onError(Throwable t) {
            if (Log.isLoggable(TAG, Log.ERROR))
                Log.e(TAG, "SSE error", t);
        }

        @Override
        public void onClosed(boolean willReconnect) {
            Log.v("SSE Closed", "reconnect? " + willReconnect);
        }
    }

    public void updateList (){
        // Change tweet list here with iterator
        tweets.clear();

        JsonNode obj = data.get("messages");
        Tweet tweet = null;
        User user= null;
        String date, body, imgurl;

        for (Iterator<JsonNode> iterator = obj.iterator(); iterator.hasNext(); ){
            JsonNode tweetJson = iterator.next();

            // creating user
            user = new User(
                    "@"+tweetJson.get("user").get("username").asText(),
                    tweetJson.get("user").get("name").asText(),
                    StringEscapeUtils.unescapeHtml4(tweetJson.get("user").get("bio").asText()),
                    tweetJson.get("user").get("join_date").asText(),
                    tweetJson.get("user").get("avatar_url").asText(),
                    tweetJson.get("user").get("location").asText(),
                    tweetJson.get("user").get("followers").asText(),
                    tweetJson.get("user").get("following").asText(),
                    tweetJson.get("user").get("identity").asText(),
                    tweetJson.get("user").get("trading_strategy")
                            .get("experience").asText(),
                    tweetJson.get("user").get("trading_strategy")
                            .get("holding_period").asText(),
                    tweetJson.get("user").get("trading_strategy")
                            .get("approach").asText(),
                    tweetJson.get("user").get("website_url").asText(),
                    tweetJson.get("user").get("ideas").asText()
                    );


            // created_at gives "[date]T[time]Z". Note: T = Tag and Z = Zeit
            date = tweetJson.get("created_at").asText()
                    .replace("T", " at ").replace("Z", "");

            // unescape the HTML in the text
            body = StringEscapeUtils.unescapeHtml4(tweetJson.get("body").asText());

            if(!tweetJson.path("entities").path("chart").path("large").isMissingNode()){
                // here you can get "thumb", "large" or "original" picture in "chart" object node
                // we display "large" instead of "original", often too heavy, sometimes a gif and given to GC
                imgurl = tweetJson.path("entities").path("chart").path("large").asText();
            } else {
                imgurl = "";
            }

            // Create new tweet object
            tweet = new Tweet(
                    body,
                    date,
                    imgurl,
                    user
            );
            tweets.add(tweet);
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.updateData(tweets);
                adapter.notifyDataSetChanged();
            }
        });
    }

    /* ****************************** MYLISTAdapter ****************************** */
    public class MyListAdapter extends BaseAdapter {
        private final LayoutInflater inflater;
        private ArrayList<Tweet> tweets;


        /**
         * our ctor for this adapter, we'll accept all the things we need here
         *
         * @param tweets
         */
        public MyListAdapter(final Context context, ArrayList<Tweet> tweets) {
            this.tweets = tweets;
            inflater = LayoutInflater.from(context);
        }

        public void updateData(ArrayList<Tweet> tweets){
            this.tweets = new ArrayList<Tweet>(tweets);
        }

        @Override
        public int getCount() {
            return tweets != null ? tweets.size() : 0;
        }

        @Override
        public Object getItem(int i) {
            return tweets != null ? tweets.get(i) : null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewWrapper viewWrapper;
            Picasso mPicasso = Picasso.with(MainActivity.this);
            // Next line allows visual tracking of image source in ui (net, disk or mem)
            // mPicasso.setIndicatorsEnabled(true);
            // NOTE: red ribbon= network, green ribbon= memory, purple ribbon= disk
            if(convertView == null) {
                viewWrapper = new ViewWrapper();
                convertView = inflater.inflate(R.layout.stocktweet, null);
                viewWrapper.tweet = (TextView) convertView.findViewById(R.id.tweet);
                viewWrapper.name = (TextView) convertView.findViewById(R.id.name);
                viewWrapper.uname = (TextView) convertView.findViewById(R.id.username);
                viewWrapper.date = (TextView) convertView.findViewById(R.id.date_time);
                viewWrapper.avatar = (ImageView) convertView.findViewById(R.id.avatar);
                viewWrapper.img = (ImageView) convertView.findViewById(R.id.tweet_img);
                convertView.setTag(viewWrapper);
            } else {
                viewWrapper = (ViewWrapper) convertView.getTag();
            }

            if(tweets != null){
                // Set ui here
                viewWrapper.tweet.setText(tweets.get(position).body);
                viewWrapper.name.setText(tweets.get(position).user.name);
                viewWrapper.uname.setText(tweets.get(position).user.username);
                viewWrapper.date.setText(tweets.get(position).date_time);
                Ion.with(viewWrapper.avatar)
                        .placeholder(R.drawable.defaulticon)
                        .error(R.drawable.defaulticon)
                        .load(tweets.get(position).user.avatarURL);
                if (tweets.get(position).imgURL.isEmpty()){
                    // Here we avoid loading a default picture from network or disk memory
                    // and leave the imageView empty
                    viewWrapper.img.setImageDrawable(null);
                } else {
                    mPicasso.with(MainActivity.this)
                            .load(tweets.get(position).imgURL)
                            .into(viewWrapper.img);
                }
            }
            return convertView;
        }
    }
    /* **************************** Class ViewWrapper **************************** */
    private static class ViewWrapper {
        TextView tweet;
        TextView name;
        TextView uname;
        TextView date;
        ImageView avatar;
        ImageView img;
    }


    /* ******************************* Class Tweet ******************************* */
    private class Tweet{
        public String body;
        public String date_time;
        public String imgURL;
        public User user;

        public Tweet(String body_, String date_time_, String imgUrl_, User u){
            this.body = body_;
            this.date_time = date_time_; // time is GMT+0
            this.imgURL = imgUrl_;
            this.user = u;
        }
    }

    /* ******************************* Class User ******************************* */
    private class User{
        public String username;
        public String name;
        public String bio;
        public String join;
        public String avatarURL;
        public String location;
        public String followers;
        public String following;
        public String off;
        public String xplvl;
        public String holding_p;
        public String approach;
        public String webURL;
        public String ideas;

        public User (String uname, String name, String bio, String joined, String avatar, String loc,
                    String followers, String following, String off, String xp,
                    String holding_p, String approach, String weburl, String Ideas) {
            this.username = uname;
            this.name = name;
            this.bio = bio;
            this.join = joined;
            this.avatarURL = avatar;
            this.location = loc;
            this.followers = followers;
            this.following = following;
            this.off = off;
            this.xplvl = xp;
            this.holding_p= holding_p;
            this.approach = approach;
            this.webURL = weburl;
            this.ideas= Ideas;
        }

    }


    /* ******************************* TOOLBAR ************************************ */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
